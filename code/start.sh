#!/bin/bash
python manage.py migrate
gunicorn --env DJANGO_SETTINGS_MODULE=project.production_settings project.wsgi
nginx -g 'daemon off;'