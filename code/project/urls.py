"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from apps.registrados.views import RegisterView, ParticipantResponseView, \
    AmountResponseView, CountriesView, CitiesView, DownloadView
from apps.trabajos.views import WorksView
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RegisterView.as_view()),
    url(r'^participant-response/$', ParticipantResponseView.as_view()),
    url(r'^amount-response/$', AmountResponseView.as_view()),
    url(r'^countries/$', CountriesView.as_view(), name="countries"),
    url(r'^cities/$', CitiesView.as_view(), name="cities"),
    url(r'^works/$', WorksView.as_view(), name="works"),
    url(r'^download/(.*)$', DownloadView.as_view(), name="download"),
]
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
