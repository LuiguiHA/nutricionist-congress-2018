from django.db import models
import arrow
from datetime import date

class PlanEvent(models.Model):

    name = models.CharField(
        max_length=100
    )
    price1 = models.FloatField(
        null=True
    )

    price2 = models.FloatField(
        null=True
    )

    price3 = models.FloatField(
        null=True
    )

    early_1_date = models.DateField()

    early_2_date = models.DateField()

    def __str__(self):
        return self.name


class RegistrationActivities(models.Model):
    name = models.CharField(
        max_length=100
    )
    price = models.FloatField(
        null=True
    )


class Participant(models.Model):

    first_name = models.CharField(
        max_length=100,
        verbose_name="Nombres"
    )
    last_name = models.CharField(
        max_length=100,
        verbose_name="Apellidos"
    )
    country = models.CharField(
        max_length=100,
        verbose_name="País"
    )
    city = models.CharField(
        max_length=100,
        verbose_name="Ciudad"
    )
    document = models.CharField(
        max_length=15,
        verbose_name="DNI o Pasaporte"
    )
    email = models.EmailField(
        verbose_name="Correo"
    )
    phone = models.CharField(
        max_length=20,
        verbose_name="Teléfono"
    )
    profession = models.CharField(
        max_length=100,
        verbose_name="Profesión"
    )
    tuition = models.CharField(
        null=True,
        max_length=100,
        verbose_name="Número de colegiatura"
    )
    plan_type = models.ForeignKey(
        PlanEvent,
        verbose_name="Tipo de Plan"
    )
    confirmation_code = models.CharField(
        max_length=100,
        verbose_name="Código de registro",
        null=True
    )
    pregrade_student = models.BooleanField(
        default=False,
        verbose_name="Estudiante de Pregrado"
    )
    go_pre_congress = models.BooleanField(
        default=False
    )
    go_workshop_1 = models.BooleanField(
        default=False
    )
    go_workshop_2 = models.BooleanField(
        default=False
    )
    go_workshop_3 = models.BooleanField(
        default=False
    )
    go_workshop_4 = models.BooleanField(
        default=False
    )
    go_workshop_5 = models.BooleanField(
        default=False
    )
    go_workshop_6 = models.BooleanField(
        default=False
    )
    created_at = models.DateTimeField(
       null=True,
       auto_now_add=True,
       editable=False,
       verbose_name="Fecha de registro"
    )
    updated_at = models.DateTimeField(
       null=True,
       auto_now=True,
       editable=False
    )

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta():
        verbose_name = "Participante"
        verbose_name_plural = "Participantes"

    def tuition_name(self):
        if self.tuition:
            return self.tuition
        else:
            return "-"
    tuition_name.short_description = "Nùmero de colegiatura"

    def type_plan(self):
        return self.plan_type.name
    type_plan.short_description = "Tipo de Plan"

    def total_price(self):
        return '$ ' + '{:.2f}'.format(self.register_payment.first().amount)
    total_price.short_description = "Monto total"

    def plan_price(self):
        now_date = self.created_at.date()
        plan_event = self.plan_type
        if now_date <= plan_event.early_1_date:
            ticket_price = plan_event.price1
        elif plan_event.early_1_date < now_date <= plan_event.early_2_date:
            ticket_price = plan_event.price2
        else:
            ticket_price = plan_event.price3

        return '$ ' + '{:.2f}'.format(ticket_price)

    plan_price.short_description = "Precio del Plan"

    def activities(self):
        cadena = ""
        price = 50
        price = '$ ' + '{:.2f}'.format(price)
        if self.go_pre_congress:
            cadena = cadena + "Pre Congreso - "
            now_date = self.created_at.date()
            plan_event = self.plan_type
            if now_date <= plan_event.early_1_date:
                price1 = 0
                price1 = '$ ' + '{:.2f}'.format(price1)
                cadena = cadena + price1 + "<br>"
            else:
                cadena = cadena + price + "<br>"

        if self.go_workshop_1:
            cadena = cadena + "Taller Nº 01 - " + price + "<br>"

        if self.go_workshop_2:
            cadena = cadena + "Taller Nº 02 - " + price + "<br>"

        if self.go_workshop_3:
            cadena = cadena + "Taller Nº 03 - " + price + "<br>"

        if self.go_workshop_4:
            cadena = cadena + "Taller Nº 04 - " + price + "<br>"

        if self.go_workshop_5:
            cadena = cadena + "Taller Nº 05 - " + price + "<br>"

        if self.go_workshop_6:
            cadena = cadena + "Taller Nº 06 - " + price + "<br>"

        if len(cadena) == 0:
            return "Sin actividades"
        return cadena

    activities.short_description = "Actividades"
    activities.allow_tags = True


class RegisterPaymentInfo(models.Model):

    amount = models.IntegerField()
    transaction_id = models.CharField(
        max_length=100,
        verbose_name="Código de transacción"
    )
    transaction_date = models.DateTimeField(
        verbose_name="Fecha de transacción"
    )
    card_number = models.CharField(
        max_length=100,
        verbose_name="Número de tarjeta"
    )
    card_number_last_four = models.CharField(
        max_length=4,
        verbose_name="Últimos 4 dígitos"
    )
    card_country = models.CharField(
        max_length=100,
        verbose_name="País de tarjeta"
    )
    card_country_code = models.CharField(
        max_length=100,
        verbose_name="Código del país de tarjeta"
    )
    card_brand = models.CharField(
        max_length=100,
        verbose_name="Marca de tarjeta"
    )
    card_type = models.CharField(
        max_length=100,
        verbose_name="Tipo de tarjeta"
    )
    card_category = models.CharField(
        null=True,
        max_length=100,
        verbose_name="Categoría de tarjeta"
    )
    card_bank = models.CharField(
        max_length=100,
        verbose_name="Banco de tarjeta"
    )
    card_bank_country = models.CharField(
        max_length=100,
        verbose_name="País del banco de tarjeta"
    )
    card_bank_country_code = models.CharField(
        max_length=100,
        verbose_name="Código del país del banco de tarjeta"

    )
    client_ip = models.CharField(
        max_length=100,
        verbose_name="IP del registrado"
    )
    registered = models.ForeignKey(
        Participant,
        related_name='register_payment'
    )

    def get_price(self):
        return '$' + '{:.2f}'.format(self.amount)
    get_price.short_description = 'Precio'

    def category_name(self):
        if self.card_category:
            return self.card_category
        else:
            return '-'
    category_name.short_description = 'Categoría de tarjeta'

    def __str__(self):
        return ""

    class Meta():
        verbose_name = "Registro de pago"
        verbose_name_plural = "Registros de pago"


class Country(models.Model):
    name = models.CharField(
        max_length=100
    )
    code = models.CharField(
        max_length=2,
        unique=True
    )


class City(models.Model):
    name = models.CharField(
        max_length=100
    )
    country = models.ForeignKey(
        Country
    )
