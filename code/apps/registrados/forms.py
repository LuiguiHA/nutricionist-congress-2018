from django import forms
from .models import RegisterPaymentInfo
from django.conf import settings

class RegisterPaymentInfoForm(forms.ModelForm):
    price = forms.CharField(
        label = "Monto cargado",
    )

    class Media:
       css = {'all': (settings.STATIC_URL + '/admin/css/main.css',)}

    class Meta:
        model = RegisterPaymentInfo
        exclude = ["amount"]

    def __init__(self, *args, **kwargs):

        super(RegisterPaymentInfoForm, self).__init__(*args, **kwargs)

        if self.instance.pk:
            self.fields['price'].widget.attrs['readonly'] = True
            price_plan = self.instance.registered.total_price()
            self.initial['price'] = price_plan
