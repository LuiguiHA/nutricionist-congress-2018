# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-02-05 17:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registrados', '0007_auto_20180205_1124'),
    ]

    operations = [
        migrations.AddField(
            model_name='participant',
            name='go_workshop_5',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='participant',
            name='go_workshop_6',
            field=models.BooleanField(default=False),
        ),
    ]
