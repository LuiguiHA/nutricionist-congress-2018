from django.contrib import admin
from .models import Participant, RegisterPaymentInfo
from .forms import RegisterPaymentInfoForm
import django_excel as excel
from django.conf.urls import url
# Register your models here.


class RegisterPaymentInfoInline(admin.StackedInline):

    model = RegisterPaymentInfo
    extra = 0
    form = RegisterPaymentInfoForm
    fields = ('transaction_date', 'card_number',
            'card_number_last_four', 'transaction_id',
            'card_country', 'card_country_code', 'card_brand',
            'card_type', 'category_name', 'card_bank',
            'card_bank_country', 'card_bank_country_code',
            'client_ip')
    readonly_fields = ('transaction_date', 'card_number',
                       'card_number_last_four', 'transaction_id',
                       'card_country', 'card_country_code', 'card_brand',
                       'card_type', 'category_name', 'card_bank',
                       'card_bank_country', 'card_bank_country_code',
                       'client_ip')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Participant)
class ParticipantAdmin(admin.ModelAdmin):

    inlines = [RegisterPaymentInfoInline]
    search_fields = ['first_name', 'last_name', 'document',
                     'email', 'confirmation_code']
    list_filter = ["country", "city", "plan_type"]

    list_display = ["first_name", "last_name", "email", "phone", "country",
                    "city", "document", "type_plan",
                    "total_price", "confirmation_code", "created_at"]

    fields = ("first_name", "last_name", "email", "phone", "country", "city",
              "document", "profession", "tuition_name", "type_plan",
              "plan_price", "activities")

    readonly_fields = (
        "first_name", "last_name", "email", "phone", "country",
        "city", "document", "profession", "tuition_name", "type_plan",
        "plan_price", "activities")

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return False
        else:
            return True

    def has_add_permission(self, request):
        if request.user.is_superuser:
            return False

    def participant_export(self, request):
        all_data = []
        list_header = [
            "Nombres",
            "Apellidos",
            "Correo",
            "Teléfono",
            "País",
            "Ciudad",
            "DNI o Pasaporte",
            "Profesión",
            "Número de colegiatura",
            "Estudiante de Pregrado",
            "Código de confirmación",
            "Fecha",
            "Tipo de plan",
            "Precio de plan (en dólares)",
            "Pre Congreso ($50.00 después del 31 de marzo)",
            "Taller Nº 01 ($50.00)",
            "Taller Nº 02 ($50.00)",
            "Taller Nº 03 ($50.00)",
            "Taller Nº 04 ($50.00)",
            "Taller Nº 05 ($50.00)",
            "Taller Nº 06 ($50.00)",
            "Monto total (en dólares)"]
        all_data.append(list_header)
        participants = Participant.objects.all()
        for participant in participants:
            row = [
                participant.first_name.capitalize(),
                participant.last_name.capitalize(),
                participant.email,
                participant.phone,
                participant.country,
                participant.city,
                participant.document,
                participant.profession.capitalize(),
                participant.tuition_name(),
                "Si" if participant.pregrade_student else "No",
                participant.confirmation_code,
                participant.created_at,
                participant.type_plan(),
                float(participant.plan_price()[2:]),
                "Si" if participant.go_pre_congress else "No",
                "Si" if participant.go_workshop_1 else "No",
                "Si" if participant.go_workshop_2 else "No",
                "Si" if participant.go_workshop_3 else "No",
                "Si" if participant.go_workshop_4 else "No",
                "Si" if participant.go_workshop_5 else "No",
                "Si" if participant.go_workshop_6 else "No",
                float(participant.total_price()[2:]),
            ]
            all_data.append(row)
        return excel.make_response_from_array(all_data, 'xlsx', file_name="Participantes")

    def get_urls(self):
        urls = super(ParticipantAdmin, self).get_urls()
        saloon_register_urls = [
            url(r'^participant-export/$', self.admin_site.admin_view(self.participant_export), name="participant-export"),
        ]
        return saloon_register_urls + urls
