from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseNotFound, HttpResponse, Http404
from django.conf import settings
from django.contrib import messages
from .models import Country, City
from .utils import ValidateCulqi, ParticipantResponse, AmountResponse
import uuid
import json
import culqipy
import os

# Create your views here.
culqipy.public_key = os.environ['CULQI_PUBLIC_KEY']
culqipy.secret_key = os.environ['CULQI_PRIVATE_KEY']


class RegisterView(View):
    template_name = "index.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RegisterView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = request.POST
        response = ValidateCulqi(data, request)

        return JsonResponse(response)


class ParticipantResponseView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ParticipantResponseView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = request.POST
        response = ParticipantResponse(data)
        return JsonResponse(response)


class AmountResponseView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AmountResponseView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        response = AmountResponse(data)
        price = '$' + '{:.2f}'.format(response)

        return JsonResponse({'plan_price': price})


class CountriesView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CountriesView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        countries = list(Country.objects.all().values('id', 'name'))
        return JsonResponse({'countries': countries})


class CitiesView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CitiesView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = request.GET
        country_id = data.get('country_id')
        cities = list(City.objects.filter(country=country_id).values('id', 'name'))
        return JsonResponse({'cities': cities})


class DownloadView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DownloadView, self).dispatch(request, *args, **kwargs)

    def get(self, request, filename, *args, **kwargs):
        filename = os.path.join(os.path.join(settings.BASE_DIR, 'files'), filename)
        try:
            f = open(filename, 'rb')
            response = HttpResponse(f.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(filename)
            return response
        except Exception as e:
            raise Http404
