from .models import PlanEvent, RegisterPaymentInfo, Participant, Country, \
                    City, RegistrationActivities
from django.http import JsonResponse
from datetime import datetime
import culqipy
import arrow
import random
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from hashids import Hashids
from dateutil import tz


def PricePlanCalculate(plan_event):
    now_date = arrow.now('America/Lima').date()
    if now_date <= plan_event.early_1_date:
        ticket_price = plan_event.price1
    elif plan_event.early_1_date < now_date <= plan_event.early_2_date:
        ticket_price = plan_event.price2
    else:
        ticket_price = plan_event.price3

    return ticket_price


def AmountResponse(data):
    total_price = 0
    plan_id = data.get('plan_id')
    plan_event = PlanEvent.objects.get(id=plan_id)
    ticket_price = PricePlanCalculate(plan_event)
    total_price = total_price + ticket_price
    if data.get('pre_congress'):
        now_date = arrow.now('America/Lima').date()
        if now_date > plan_event.early_1_date:
            pre_congress = RegistrationActivities.objects.get(name='pre-congress')
            pre_congress_price = pre_congress.price
            total_price = total_price + pre_congress_price

    if data.get('taller_1'):
        taller_1 = RegistrationActivities.objects.get(name='taller_1')
        taller_1_price = taller_1.price
        total_price = total_price + taller_1_price

    if data.get('taller_2'):
        taller_2 = RegistrationActivities.objects.get(name='taller_2')
        taller_2_price = taller_2.price
        total_price = total_price + taller_2_price

    if data.get('taller_3'):
        taller_3 = RegistrationActivities.objects.get(name='taller_3')
        taller_3_price = taller_3.price
        total_price = total_price + taller_3_price

    if data.get('taller_4'):
        taller_4 = RegistrationActivities.objects.get(name='taller_4')
        taller_4_price = taller_4.price
        total_price = total_price + taller_4_price

    if data.get('taller_5'):
        taller_5 = RegistrationActivities.objects.get(name='taller_5')
        taller_5_price = taller_5.price
        total_price = total_price + taller_5_price

    if data.get('taller_6'):
        taller_6 = RegistrationActivities.objects.get(name='taller_6')
        taller_6_price = taller_6.price
        total_price = total_price + taller_6_price

    return total_price


def Activities(participant):
    activities = []
    if participant.go_pre_congress:
        activity = RegistrationActivities.objects.get(name='pre-congress')
        now_date = arrow.now('America/Lima').date()
        if now_date <= participant.plan_type.early_1_date:
            price = '$' + '{:.2f}'.format(0)
            activities.append({'name': 'Pre Congreso',
                               'price': price})
        else:
            price = '$' + '{:.2f}'.format(activity.price)
            activities.append({'name': 'Pre Congreso',
                               'price': price})
    if participant.go_workshop_1:
        activity = RegistrationActivities.objects.get(name='taller_1')
        price = '$' + '{:.2f}'.format(activity.price)
        activities.append({'name': 'Taller Nº 01',
                           'price': price})

    if participant.go_workshop_2:
        activity = RegistrationActivities.objects.get(name='taller_2')
        price = '$' + '{:.2f}'.format(activity.price)
        activities.append({'name': 'Taller Nº 02',
                           'price': price})

    if participant.go_workshop_3:
        activity = RegistrationActivities.objects.get(name='taller_3')
        price = '$' + '{:.2f}'.format(activity.price)
        activities.append({'name': 'Taller Nº 03',
                           'price': price})

    if participant.go_workshop_4:
        activity = RegistrationActivities.objects.get(name='taller_4')
        price = '$' + '{:.2f}'.format(activity.price)
        activities.append({'name': 'Taller Nº 04',
                           'price': price})

    if participant.go_workshop_5:
        activity = RegistrationActivities.objects.get(name='taller_5')
        price = '$' + '{:.2f}'.format(activity.price)
        activities.append({'name': 'Taller Nº 05',
                          'price': price})

    if participant.go_workshop_6:
        activity = RegistrationActivities.objects.get(name='taller_6')
        price = '$' + '{:.2f}'.format(activity.price)
        activities.append({'name': 'Taller Nº 06',
                           'price': price})

    return activities


def ParticipantResponse(participant):

    register = RegisterPaymentInfo.objects.get(registered=participant)
    ticket_price = PricePlanCalculate(participant.plan_type)
    participant_dict = {
        'email': participant.email,
        'amount': '$' + '{:.2f}'.format(register.amount),
        'ticket_type': participant.plan_type.name,
        'ticket_price': '$' + '{:.2f}'.format(ticket_price),
        'activities': Activities(participant),
        'transaction_id': register.transaction_id,
        'transaction_date': arrow.get(
            register.transaction_date, tz.gettz('America/Lima')
            ).shift(hours=-5).format('YYYY-MM-DD HH:mm:ss')
    }
    return participant_dict


def EmailConfirmation(participant, confirmation_code):
    email = participant.email
    register = RegisterPaymentInfo.objects.get(registered=participant)
    ticket_price = PricePlanCalculate(participant.plan_type)
    email_template = render_to_string(
        "emails/confirm_payment.html",
        {
            'participant': participant,
            'confirmation_code': confirmation_code,
            'ticket_type': participant.plan_type.name,
            'ticket_price': '$' + '{:.2f}'.format(ticket_price),
            'date': arrow.get(
                register.transaction_date, tz.gettz('America/Lima')
                ).shift(hours=-5).format('YYYY-MM-DD HH:mm:ss'),
            'total_price': '$' + '{:.2f}'.format(register.amount),
            'activities': Activities(participant)
        }
    )

    subject = 'Confirmación de registro al XVII Congreso CONFELANYD'

    send_mail(
        subject,
        '',
        settings.DEFAULT_FROM_EMAIL,
        [email],
        fail_silently=False,
        html_message=email_template,
    )


def ValidateCulqi(data, request):
    print(data)
    country_id = data.get('country')
    country = Country.objects.get(id=country_id)
    try:
        city = data.get('city')
        city = City.objects.get(id=int(city)).name
    except:
        city = data.get('city')

    charge_dict = {
        'card_brand': data.get('card_brand'),
        'amount': int(data.get('amount')) * 100,
        'currency_code': 'USD',
        'description': 'Registro',
        'email': data.get('email'),
        'installments': 0,
        'source_id': data.get('token_id'),
        'antifraud_details': {
            'address_city': city,
            'country_code': country.code,
            'first_name': data.get('first_name'),
            'last_name': data.get('last_name'),
            'phone_number': str(data.get('phone'))
        },
        'metadata': {
            'Teléfono': data.get('phone'),
            'Ciudad': city,
            'Nombres': data.get('first_name'),
            'Apellidos': data.get('last_name'),
            'Email': data.get('email'),
            'Profesión': data.get('profession'),
            'País': country.name,
            'Dni': data.get('dni')
        }
    }

    charge = culqipy.Charge.create(charge_dict)

    if charge.get('object') == 'charge':
        participant = Participant()
        participant.first_name = data.get('first_name')
        participant.last_name = data.get('last_name')
        participant.phone = str(data.get('phone'))
        participant.city = city
        participant.country = country.name
        participant.email = data.get('email')
        participant.document = data.get('dni')
        participant.profession = data.get('profession')
        participant.tuition = data.get('tuition')
        if data.get('student') == "0":
            participant.pregrade_student = False
        else:
            participant.pregrade_student = True

        plan_event = PlanEvent.objects.get(id=int(data.get('plan')))
        participant.plan_type = plan_event

        hashids = Hashids(min_length=6)
        confirmation_code = hashids.encode(random.randrange(0, 101, 2)).upper()
        participant.confirmation_code = confirmation_code

        go_pre_congress = data.get('pre_congress') is not None
        participant.go_pre_congress = go_pre_congress
        go_workshop_1 = data.get('taller_1') is not None
        participant.go_workshop_1 = go_workshop_1
        go_workshop_2 = data.get('taller_2') is not None
        participant.go_workshop_2 = go_workshop_2
        go_workshop_3 = data.get('taller_3') is not None
        participant.go_workshop_3 = go_workshop_3
        go_workshop_4 = data.get('taller_4') is not None
        participant.go_workshop_4 = go_workshop_4
        go_workshop_5 = data.get('taller_5') is not None
        participant.go_workshop_5 = go_workshop_5
        go_workshop_6 = data.get('taller_6') is not None
        participant.go_workshop_6 = go_workshop_6

        participant.save()

        register = RegisterPaymentInfo()
        register.amount = data.get('amount')
        register.transaction_id = charge.get('id')
        creation_date = int(charge.get('creation_date')) / 1000
        creation_date = datetime.fromtimestamp(creation_date).strftime('%Y-%m-%d %H:%M:%S')
        register.transaction_date = creation_date
        register.card_number = charge.get('source').get('card_number')
        register.card_number_last_four = charge.get('source').get('last_four')
        register.card_country = charge.get('source').get('client').get('ip_country')
        register.card_country_code = charge.get('source').get('client').get('ip_country_code')
        register.card_brand = charge.get('source').get('iin').get('card_brand')
        register.card_type = charge.get('source').get('iin').get('card_type')
        register.card_category = charge.get('source').get('iin').get('card_category')

        card_bank = charge.get('source').get('iin').get('issuer').get('name')
        register.card_bank = card_bank if card_bank else "No se obtuvo el banco"

        card_bank_country = charge.get('source').get('iin').get('issuer').get('country')
        register.card_bank_country = card_bank_country if card_bank_country else "No se obtuvo el pais del banco"

        card_bank_country_code = charge.get('source').get('iin').get('issuer').get('country_code')
        register.card_bank_country_code = card_bank_country_code if card_bank_country_code else "No se obtuvo el codigo de pais del banco"

        client_ip = charge.get('source').get('client').get('ip')
        register.client_ip = client_ip if client_ip else "No se obtuvo la ip del cliente"
        
        register.registered = participant
        register.save()

        EmailConfirmation(participant, confirmation_code)
        participant_response = ParticipantResponse(participant)
        return {'success': True, 'participant': participant_response}

    else:
        print(charge)
        message = "Error en el pago (code: 1000)"

        if charge.get('type') == 'parameter_error':
            message = "Error de parámetro de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1001)"

        if charge.get('type') == 'invalid_request_error':
            message = "Ocurrió un error inesperado. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1002)"

        if charge.get('type') == 'authentication_error':
            message = "Ocurrió un error inesperado procesando el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1003)"

        if charge.get('type') == 'limit_api_error':
            message = "Ocurrió un error con la pasarela de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1005)"

        if charge.get('type') == 'resource_error':
            message = "Ocurrió un error con la pasarela de pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1005)"

        if charge.get('type') == 'api_error':
            message = "Ocurrió un error inesperado procesando el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 1007)"

        if charge.get('type') == 'card_error':

            if charge.get('decline_code'):

                if charge.get('decline_code') == 'insufficient_funds':
                    message = "Fondos insuficientes. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2001)"

                if charge.get('decline_code') == 'expired_card':
                    message = "Tarjeta vencida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2002)"

                if charge.get('decline_code') == 'stolen_card':
                    message = "La tarjeta ha sido reportada como perdida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2004)"

                if charge.get('decline_code') == 'contact_issuer':
                    message = "El banco ha rechazado la transacción, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2005)"

                if charge.get('decline_code') == 'invalid_cvv':
                    message = "Código CVV inválido. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2006)"

                if charge.get('decline_code') == 'incorrect_cvv':
                    message = "Código CVV incorrecto. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2007)"

                if charge.get('decline_code') == 'too_many_attempts_cvv':
                    message = "Demasiados intentos para ingresar el código CVV. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2008)"

                if charge.get('decline_code') == 'issuer_not_available':
                    message = "El banco no responde, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2009)"

                if charge.get('decline_code') == 'issuer_decline_operation':
                    message = "El banco ha denegado la transacción, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2010)"

                if charge.get('decline_code') == 'invalid_card':
                    message = "La tarjeta no está permitida para este tipo de transacciones, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2011)"

                if charge.get('decline_code') == 'processing_error':
                    message = "Ocurrió un error inesperado mientras se procesaba el pago. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2012)"

                if charge.get('decline_code') == 'fraudulent':
                    message = "Transacción sospechosa, por favor contacta a tu banco. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2013)"

                if charge.get('decline_code') == 'culqi_card':
                    message = "Tarjeta no permitida. Si el problema persiste por favor ponte en contacto con nosotros. (code: 2014)"

            if charge.get('code'):

                if charge.get('code') == 'card_declined':
                    message = "Tarjeta rechazada. (code: 2000)"
                else:
                    message: "Ocurrío un error con la tarjeta (code: 2015)"

        return {'success': False, 'message': message}
