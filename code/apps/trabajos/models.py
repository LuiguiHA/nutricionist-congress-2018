from django.db import models

# Create your models here.


class Works(models.Model):
    first_name = models.CharField(
        max_length=100,
        verbose_name="Nombres"
    )
    last_name = models.CharField(
        max_length=100,
        verbose_name="Apellidos"
    )
    document = models.CharField(
        max_length=15,
        verbose_name="DNI"
    )
    country = models.CharField(
        max_length=100,
        verbose_name="País"
    )
    city = models.CharField(
        max_length=100,
        verbose_name="Ciudad"
    )
    theme = models.CharField(
        max_length=100,
        verbose_name="Tema de trabajo"
    )
    file_work = models.FileField(
        upload_to='works',
        verbose_name='Archivo'
    )
    created_at = models.DateTimeField(
       null=True,
       auto_now_add=True,
       editable=False,
       verbose_name="Fecha de registro"
    )

    updated_at = models.DateTimeField(
       null=True,
       auto_now=True,
       editable=False
    )

    class Meta():
        verbose_name = "Trabajo"
        verbose_name_plural = "Trabajos"
