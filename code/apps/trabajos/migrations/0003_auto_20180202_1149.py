# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-02-02 16:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trabajos', '0002_auto_20180202_1147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='works',
            name='theme',
            field=models.CharField(max_length=100, verbose_name='Tema de trabajo'),
        ),
    ]
