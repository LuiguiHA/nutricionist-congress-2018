from .models import Works
from apps.registrados.models import Country, City


def saveWork(data, file_work):
    country_id = int(data.get('country'))
    country = Country.objects.get(id=country_id)
    try:
        city = data.get('city')
        city = City.objects.get(id=int(city)).name
    except:
        city = data.get('city')

    work = Works()
    work.first_name = data.get('name')
    work.last_name = data.get('last_name')
    work.document = data.get('document')
    work.country = country.name
    work.city = city
    work.theme = data.get("theme").capitalize()
    work.file_work = file_work
    work.save()
