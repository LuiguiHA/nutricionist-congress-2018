from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.contrib import messages
from apps.registrados.models import Country, City
from .utils import saveWork
import uuid
import json
import culqipy
import os


# Create your views here.


class WorksView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(WorksView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = request.POST
        file_work = request.FILES
        saveWork(data, file_work.get('file'))
        print("========")
        print(data)
        print(file_work)
        print(file_work.get('file'))
        # response = ValidateCulqi(data, request)

        return JsonResponse({'success': True})
